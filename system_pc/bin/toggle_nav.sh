##########################################################################################
# Android 11 Navigation Mode Selection
# Root Only
##########################################################################################

echo "!!! THIS SCRIPT REQUIRES ROOT AND FOR YOUR SYSTEM TO BE MOUNTED AS RW !!!"
echo " "
echo " "
echo "   ----- Disable Navbar? ------"
echo " "
echo "   Y = Yes, N- = No"
read input
if [[ $input == "Y" || $input == "y" ]]; then
  echo "Yes"
  setprop qemu.hw.mainkeys 1
  NB=false
  echo "OK, disabling now..."
  # soft reboot
  x=5
  while [ $x -gt 0 ]
  do
    sleep 1s
    clear
    echo "soft-rebooting in $x seconds"
    x=$(( $x - 1 ))
  done
  service call activity 42 s16 com.android.systemui
  am startservice -n com.android.systemui/.SystemUIService
else
  echo "n"
  setprop qemu.hw.mainkeys 0
  # soft reboot
  x=5
  while [ $x -gt 0 ]
  do
    sleep 1s
    clear
    echo "soft-rebooting in $x seconds"
    x=$(( $x - 1 ))
  done
  service call activity 42 s16 com.android.systemui
  am startservice -n com.android.systemui/.SystemUIService
fi
