#!/bin/bash

rom_fp="$(date +%y%m%d)"
rompath=$(pwd)
vendor_path="android-generic"
mkdir -p release/$rom_fp/
set -e

#setup colors
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
purple=`tput setaf 5`
teal=`tput setaf 6`
light=`tput setaf 7`
dark=`tput setaf 8`
CL_CYN=`tput setaf 12`
CL_RST=`tput sgr0`
reset=`tput sgr0`

localManifestBranch="q-x86"
rom="Android-Generic"
bliss_variant=""
bliss_variant_name=""
bliss_release="n"
bliss_partiton=""
filename=""
file_size=""
clean="n"
sync="n"
patch="n"
proprietary="n"
oldproprietary="n"
romBranch=""
desktopmode="n"
ipts_drivers="n"
efi_img="n"
rpm="n"
subsync="n"
kernel="n"
kername=""
backup="n"
romname=""
vendorsetup="n"

ask() {
    # https://djm.me/ask
    local prompt default reply

    if [ "${2:-}" = "Y" ]; then
        prompt="Y/n"
        default=Y
    elif [ "${2:-}" = "N" ]; then
        prompt="y/N"
        default=N
    else
        prompt="y/n"
        default=
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read reply </dev/tty

        # Default?
        if [ -z "$reply" ]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}

if [ -z "$USER" ];then
        export USER="$(id -un)"
fi
export LC_ALL=C

if [[ $(uname -s) = "Darwin" ]];then
        jobs=$(sysctl -n hw.ncpu)
elif [[ $(uname -s) = "Linux" ]];then
        jobs=$(nproc)
fi


while test $# -gt 0
do
  case $1 in

  # Normal option processing
    -h | --help)
      echo "Usage: $0 options buildVariants extraOptions addons"
      echo "options: -s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos"
      echo "		 -p | --patch: Run the patches only"
      echo "		 -d | --desktopmode: Duild without any traditional launchers and only use Taskbar/TSL from @farmerbb"
      echo "		 -n | --vendor-setup __vendorname__ : Creates all the required folders & files to start building. "
      echo ""
      echo "buildVariants: "
      echo "aag_emulator-userdebug  "
      echo ""
      ;;
    -c | --clean)
      clean="y";
      echo "Cleaning build and device tree selected."
      ;;
    -v | --version)
      echo "Version: Bliss x86 Builder 2.0"
      echo "Updated: 10/19/2019"
      ;;
    -s | --sync)
      sync="y";
      echo "Repo syncing and patching selected."
      ;;
    -p | --patch)
      patch="y";
      echo "patching selected."
      ;;
    -d | --desktopmode)
	  desktopmode="y";
	  echo "desktop mode selected"
      ;;
	-n | --vendor-setup)
	  vendorsetup="y";
	  echo "Setting up $1 vendor..."
	  ;;
  # ...

  # Special cases
    --)
      break
      ;;
    --*)
      # error unknown (long) option $1
      ;;
    -?)
      # error unknown (short) option $1
      ;;

  # FUN STUFF HERE:
  # Split apart combined short options
    -*)
      split=$1
      shift
      set -- $(echo "$split" | cut -c 2- | sed 's/./-& /g') "$@"
      continue
      ;;

  # Done with options
    *)
      break
      ;;
  esac

  # for testing purposes:
  shift
done

if [ "$1" = "ag_emulator-userdebug" ];then
        bliss_variant=ag_emulator-userdebug;
        bliss_variant_name=ag_emulator-userdebug;

fi

echo -e ${CL_CYN}""${CL_CYN}
echo -e ${CL_CYN}" Android-Generic Project addons"${CL_RST}
echo -e ${CL_CYN}""${CL_CYN}


if [ "$2" = "foss" ];then
   export USE_OPENGAPPS=false
   export USE_FDROID=false
   export USE_FOSS=true
   export USE_GO=false
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"======-Bliss-OS(x86) Building w/ microG & F-Droid-====="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "gapps" ];then
   export USE_FOSS=false
   export USE_FDROID=false
   export USE_GO=false
   export USE_OPENGAPPS=true
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=========-Bliss-OS(x86) Building w/ OpenGapps-========="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "go" ];then
   export USE_GO=true
   export USE_FDROID=false
   export USE_FOSS=false
   export USE_OPENGAPPS=false
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=============-Bliss-OS(x86) Building w/ Go-============"${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "none" ];then
   export USE_FOSS=false
   export USE_FDROID=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=============-Bliss-OS(x86) Building Clean-============"${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "gms" ];then
   export USE_GMS=true
   export USE_FDROID=false
   export USE_FOSS=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"===========-Bliss-OS(x86) Building with GMS-==========="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "fdroid" ];then
   export USE_FDROID=true
   export USE_GMS=false
   export USE_FOSS=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=========-Bliss-OS(x86) Building with FDROID-=========="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

else
   export USE_FDROID=false
   export USE_GMS=false
   export USE_FOSS=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"======-Bliss-OS(x86) Building with no additions-======"${CL_RST}
   echo -e ${CL_CYN}"      Please consider contributing to our project"       ${CL_RST}
   echo -e ${CL_CYN}"======================================================"${CL_RST}
   echo -e ""

fi

if  [ $sync == "y" ];then
	rm -rf $rompath/.repo/local_manifests

	mkdir -p $rompath/.repo/local_manifests
	echo -e ${CL_CYN}""${CL_RST}

	# Add AOSP manifests, if another vendor is found, overwrite
	cp -r $rompath/vendor/$vendor_path/manifests/emu/stock/* $rompath/.repo/local_manifests
	
	echo "Searching for vendor..."
	while IFS= read -r agvendor_name; do
		if [ -d $rompath/vendor/$agvendor_name/ ]; then
			export ROM_IS_$agvendor_name=true
			romname=$agvendor_name
			echo "Found $agvendor!!"

			echo -e ${CL_CYN}""${CL_RST}
			echo -e ${CL_CYN}"copying $agvendor_name specific manifest files..."${CL_RST}
		  
			if [ -d $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$agvendor_name/manifest_patches/ ]; then 
				echo -e ${yellow}"This ROM requires a manifest patch for one or more repos. Applying that now"${CL_RST}
				#~ cd .repo/manifests
				vendor/${vendor_path}/manifest_prepatch_emu.sh
				#~ cd $rompath
			fi 
		  
			cp -r $rompath/vendor/$vendor_path/manifests/emu/$agvendor_name/* $rompath/.repo/local_manifests
		else
			echo ""
		fi
	done < $rompath/vendor/$vendor_path/emu_roms.lst
	
	echo -e ${CL_CYN}""${CL_RST}

	cp -r $rompath/vendor/$vendor_path/manifests/emu/local_manifests/* $rompath/.repo/local_manifests
	echo -e ${CL_CYN}""${CL_RST}
	echo -e ${CL_CYN}""${CL_RST}
	echo -e ${CL_CYN}"Syncing Files..."${CL_RST}
	echo -e ${CL_CYN}""${CL_RST}
	echo -e ${CL_CYN}""${CL_RST}
	
	repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync
	
	#~ repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync | grep -q 'non-existent project:' &> /dev/null
	
	#~ if [[ "$(repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync)" =~ "non-existent" ]] ; then
		#~ echo "Output includes 'sub string'"
		#~ cut -d ":" -f2- <<< "$remove" && sed -i "/\'"$remove"'\b/d" $rompath/vendor/$vendor_path/manifests/emu/$romname/01remove.xml
	#~ else
		
		#~ echo "output included no errors"
	#~ fi
	
	#~ if repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync | grep -q 'non-existent' && cut -d ":" -f2- <<< "$remove"; then
		#~ echo "something matched"
		#~ sed -i "/\'"$remove"'\b/d" $rompath/vendor/$vendor_path/manifests/emu/$romname/01remove.xml
	#~ else
		
		#~ echo "nothing matched"
	#~ fi

else
        echo "Not gonna sync this round"
fi


if [ $clean == "y" ];then
	echo "Cleaning up a bit"
    make clean && make clobber
fi

if [ "$vendorsetup" == "y" ]; then
	if [ "$1" != "" ]; then
		echo "Setting up $1 vendor files"
		if ask "Do you want to create the base files/folders/etc for "$1"?"; then
			echo "You chose to setup $1 for emu builds"
			
			# prepatches
			if [ -d $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_prepatches/$1 ]; then
				echo "It looks to be already setup. If this is a mistake, please remove the folder"
				echo "vendor/$vendor_path/patches/google_diff/emu_vendor_prepatches/$1 and try again"
			else
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_prepatches/$1
			fi
			
			# patches
			if [ -d $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1 ]; then
				echo "It looks to be already setup. If this is a mistake, please remove the folder"
				echo "vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1 and try again"
			else
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/patches
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/custom_patches
				touch $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/custom_patches/customizations.lst
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/overlay
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/device_overrides
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/permissions
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/permissions/system
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/permissions/system/etc
				mkdir $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/permissions/system/etc/permissions
				echo "$1" >> $rompath/vendor/$vendor_path/emu_roms.lst
				echo '$(call inherit-product-if-exists, vendor/'$1'/config/common.mk)' >> $rompath/vendor/$vendor_path/config/vendors.mk
				echo "All patch folders are setup"
			fi
			
			# manifests
			if [ -d $rompath/vendor/$vendor_path/manifests/emu/$1 ]; then
				echo "There looks to already be a manifest folder setup for this. Please remove this folder if it was created by mistake"
				echo "vendor/$vendor_path/manifests/emu/$1"
			else
				mkdir $rompath/vendor/$vendor_path/manifests/emu/$1
				cp $rompath/vendor/$vendor_path/manifests/emu/stock/* $rompath/vendor/$vendor_path/manifests/emu/$1
				echo "All manifest folders are setup"
			fi
			
			# ROM Name
			if [ -f $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/config.lst ]; then
				echo "config.lst looks to be already setup. If this is a mistake, please remove the file"
				echo "vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/config.lst and try again"
			else
				touch $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/config.lst
				read -p "What do you call this ROM? (what is the ROM's name): " vsromname
				echo $vsromname >> $rompath/vendor/$vendor_path/patches/google_diff/emu_vendor_patches/$1/config.lst
				export CURRENT_ROM=$1
				CURRENT_ROM=$1
				if ask "Do you want to create the initial commit for "$1"?"; then
					# Autogenerate commmit
					cd $rompath/vendor/$vendor_path
					git add -A && git commit --author="Jon West <electrikjesus@gmail.com>" -m "[AG][generated] Initial setup for $1"
					cd $rompath
				fi
				echo "$1 is setup"
			fi
			
		else
			echo "You chose not to setup $1 for emu builds"
		fi
	else
		echo -e ${red} "You did not supply a ROM name. Usage: build-x86 -n rom_name"${CL_RST}
	fi
fi

if [ $patch == "y" ] || [ $sync == "y" ]; then
	echo "Let the patching begin"
	bash "$rompath/vendor/$vendor_path/patch_emu.sh"
fi

while IFS= read -r agvendor_name; do
	#~ echo -e ${CL_CYN}""${CL_RST}
	#~ echo "Searching for vendor: $agvendor_name"
	if [ -d $rompath/vendor/$agvendor_name/ ]; then
		echo "Found $agvendor_name, setting that as CURRENT_ROM"
		export CURRENT_ROM=$agvendor_name		
	#~ else
	  #~ echo "Not Found"
	fi
done < $rompath/vendor/$vendor_path/emu_roms.lst
echo "CURRENT_ROM=${CURRENT_ROM}"


# Begin builds

if [[ "$1" = "ag_emulator-userdebug" ]];then
echo "Setting up build env for: $1"
	. build/envsetup.sh
fi

buildVariant() {
	#~ echo "Starting lunch command for: $1"
	#~ lunch $1
	echo "Starting up the build... This may take a while..."
	
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"======-Android-Generic Building as EMU Image -======"${CL_RST}
	echo -e ${CL_CYN}"                Enjoy!	 "       ${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""
	#~ make -j$((`nproc`-2))
	#~ make
	#~ vendor/$vendor_path/utils/emulator/create_emulator_image.sh
	create-emulator-image
}

if [[ "$1" = "ag_emulator-userdebug" ]];then
	buildVariant $bliss_variant $bliss_variant_name
fi
